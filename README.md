# Scala Evaluation #

### Set Up ###

* Pull repo down
* Recommend using IntelliJ to run
    * There is an included run configuration

### Expected output ###

Two CSV files:

* Exploded set of data for each day in 2020
* Versioned output of the original data