package com.kalibri.csv

import com.kalibri.spark.SparkManager
import org.apache.spark.sql.DataFrame

class FileReaderWriter extends SparkManager {

  def getDataFromCsvFile(filenames: String,
                         fileOptions: CsvFileOptions = CsvFileOptions()): DataFrame = {

    var dfReader = spark.read
      .format("csv")
      .option("dateFormat", fileOptions.timestampFormat)
      .option("delimiter", fileOptions.delimiter)
      .option("escape", fileOptions.escape)
      .option("header", fileOptions.header)
      .option("mode", fileOptions.errorMode.name)
      .option("multiLine", fileOptions.multiLine)
      .option("quote", fileOptions.quote)
      .option("timestampFormat", fileOptions.timestampFormat)

    if (fileOptions.schema != null) {
      dfReader = dfReader.schema(fileOptions.schema)
    } else {
      dfReader = dfReader.option("inferSchema", "true")
    }

    dfReader.load(filenames)
  }

  def writeToCsvFile(df: DataFrame, directory: String, fileOptions: CsvFileOptions = CsvFileOptions()): Unit = {

    df.coalesce(1)
      .write.format("csv")
      .mode(fileOptions.saveMode)
      .option("header", fileOptions.header)
      .option("delimiter", fileOptions.delimiter)
      .option("timestampFormat", fileOptions.timestampFormat)
      .option("dateFormat", fileOptions.timestampFormat)
      .option("quoteAll", fileOptions.quoteAll)
      .option("emptyValue", if (fileOptions.quoteAll) {
        "\"\""
      } else {
        "\u0000"
      })
      .option("escape", fileOptions.escape)
      .save(directory)
  }

}
